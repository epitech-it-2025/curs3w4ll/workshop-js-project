FROM node:19-alpine

ARG API_VERSION
ENV API_VERSION ${API_VERSION}
ENV PATH /src/node_modules/.bin:$PATH

WORKDIR /src

COPY . ./

RUN apk add bash
RUN npm install

EXPOSE 5000

CMD ["npm", "run", "start"]
